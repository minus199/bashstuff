TabsLoader V1:
	* Edit command.txt so tabsLoader will know how many tabs to load and which command run at each tab.
	* Each line in command.txt needs to be in the following format: [tab_name]<[command to run] (Note, "<" is the delimiter between the tab title to the command. Feel free to modify).
	* Friendly advice: 
		- When doing ssh or remote server commands, test before. For example, for ssh commands its advised to ssh-copy-id to the remote server if possible. See example command.txt file
		
