#!/bin/bash          

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd );

value=`cat commands.txt`
count=0
while IFS=$'\n' read -ra ADDR; do
      for i in "${ADDR[@]}"; do
	guake -n $count; #create a new tab

	count=`expr $count + 1`

	IFS='<' read -a arr <<< "$i"
	#if tabs wont rename, fix long tabs name problem on guake(where tabs are opened with long path as title by default)

	tabTitle=${arr[0]};
	guake -r $tabTitle;
	command=${arr[1]};

	notify-send --icon=$DIR"/icon.png" "Tab [$tabTitle] was created."
	

	guake -e "$command";
      done
 done <<< "$value"






